Today I would like to announce clig version 1.9.0
=
Clig now contains a Tcl-only Tcl-extension to instrument your
Tcl-scripts with a command line parser (getopt) using the same syntax
as is used to instrument C-programs.

This is supposed to be BETA software. The Tcl-package ::clig is not
yet fully documented.

**The Command Line Interpreter Generator clig**

by Harald Kirsch and Maintained by Chris Hutchinson

1. Clig creates for your C-Program or lets you use in your Tcl-script
  - a command line interpreter with those features:
````
    -- Flag, Float, Int or String options
    -- number of parameters of options can be specified
    -- range can be specified for the numerical options
    -- options can have defaults
    -- options can be mandatory (not really optional then :-)
````
        The command line interpreter will check for your C-program or
        Tcl-script the existence of mandatory options, correct range of
        numerical option arguments and correct number of
        arguments. Violation results in readable error messages.

        Parsed results are delivered to your C-program in a custum-built
        structure or as directly accessible variables to your Tcl-script.
  - a readable, up-to-date usage-message
  - a basic manual page


2. Clig takes as input a simple description file.


3. Clig generates standard C (well, I tried as hard as I can :-)
It is reported to work well with C++. Within a Tcl-script, code
generation is not necessary. Just declare your command line
arguments and then run the parser.

4. The resulting code is self contained and does not depend on a library
(except for libc, of course).

5. Clig is written in Tcl (nevertheless, (4) applies!)

6. Clig is free software.

7. More info and download at [clig at GitLab](https://gitlab.com/ports1/clig)
