########################################################################
##
## The command line parser is generated here.
##
########################################################################

########################################################################
proc genFlagParse {opt} {
  global D

  return "      cmd.$D($opt,var)P = 1;\n"
}
########################################################################
proc genRangeCheck {opt typ count var b} {
  global D

  foreach {rmin rmax} $D($opt,range) break
  set res {}
  if {![Infty $rmax]} {
    append res [b $b]
    append res "check${typ}Lower(\"$opt\", $var, $count, $rmax);\n"
  }
  if {![negInfty $rmin]} {
    append res [b $b]
    append res "check${typ}Higher(\"$opt\", $var, $count, $rmin);\n"
  }
  return $res
}
########################################################################
proc countVal {c} {
  return [expr {[Infty $c] ? -1 : $c}]
}
########################################################################
proc genXParse {opt typ} {
  global D

  append res \
      "[b 6]int keep = i;\n"\
      "[b 6]cmd.$D($opt,var)P = 1;\n"

  foreach {cmin cmax} $D($opt,count) break

  if { $cmax==1 } {
    ##### zero or one parameter
    append res \
	"[b 6]i = get${typ}Opt(argc, argv, i, " \
	                        "&cmd.$D($opt,var), $cmin);\n"\
	"[b 6]cmd.$D($opt,var)C = i-keep;\n"

    if {-1!=[lsearch {Int Long Float Double} $typ]} {
      append res \
      [genRangeCheck $opt $typ cmd.$D($opt,var)C &cmd.$D($opt,var) 6]
    }
    return $res
  } 

  ##### possibly more than one parameter
  append res \
      "[b 6]i = get${typ}Opts(argc, argv, i, &cmd.$D($opt,var), "\
                           "$cmin, [countVal $cmax]);\n"\
      "[b 6]cmd.$D($opt,var)C = i-keep;\n"


  if {-1!=[lsearch {Int Long Float Double} $typ]} {
    append res \
	[genRangeCheck $opt $typ cmd.$D($opt,var)C cmd.$D($opt,var) 6]
  }
  return $res

}
########################################################################
proc genIntParse {opt} {
  return [genXParse $opt Int]
}
########################################################################
proc genLongParse {opt} {
  return [genXParse $opt Long]
}
########################################################################
proc genFloatParse {opt} {
  return [genXParse $opt Float]
}
########################################################################
proc genDoubleParse {opt} {
  return [genXParse $opt Double]
}
########################################################################
proc genStringParse {opt} {
  return [genXParse $opt String]
}
########################################################################
proc genParse {} {
  global D haveMandatory

  ##-------------------
  set res {}
  append res \
      "Cmdline *\n"\
      "parseCmdline(int argc, char **argv)\n"\
      "\{\n"\
      "  int i;\n"

  if { $haveMandatory } {
    append res "  char missingMandatory = 0;\n"
  }

  append res \
      "\n"\
      "  Program = argv\[0\];\n"

  if [info exist D(commandline)] {
    append res \
	"  cmd.$D(commandline) = catArgv(argc, argv);\n"
  }

  append res \
      "  for(i=1, cmd.argc=1; i<argc; i++) \{\n"

  ##-------------------

  ## if there is a Rest command, `--' stops option processing
  if { [info exist D(--,type)] } {
    append res \
	"    if( 0==strcmp(\"--\", argv\[i\]) ) \{\n" \
	"      while( ++i<argc ) argv\[cmd.argc++\] = argv\[i\];\n" \
	"      continue;\n" \
	"    \}\n\n"
  }
    
  foreach opt $D(opts) {
    if {[string match "--" $opt]} continue
    append res \
	"    if( 0==strcmp(\"$opt\", argv\[i\]) ) \{\n" \
	[gen$D($opt,type)Parse $opt] \
	"      continue;\n" \
	"    \}\n\n"
  }

  append res \
      "    if( argv\[i\]\[0\]=='-' ) {\n"\
      "      fprintf(stderr, \"\\n%s: unknown option `%s'\\n\\n\",\n"\
      "              Program, argv\[i\]);\n"\
      "      usage();\n"\
      "    }\n"\
      "    argv\[cmd.argc++\] = argv\[i\];\n"\
      "  \}/* for i */\n\n"\
      "[genCheckMandatory "  "]\n"\
      "  /*@-mustfree*/\n"\
      "  cmd.argv = argv+1;\n"\
      "  /*@=mustfree*/\n"\
      "  cmd.argc -= 1;\n\n"

  #####
  ##### Check of non-option arguments (Rest-command)
  #####
  if { [info exist D(--,type)] } {
    foreach {cmin cmax} $D(--,count) break
    if { $cmin>0 } {
      append res \
	  "  if( $cmin>cmd.argc ) {\n"\
	  "    fprintf(stderr, \"%s: there should be at least"\
	  " $cmin non-option argument(s)\\n\",\n"\
	  "            Program);\n"\
	  "    exit(EXIT_FAILURE);\n"\
	  "  }\n"
    }

    if { ![Infty $cmax] } {
      append res \
	  "  if( $cmax<cmd.argc ) {\n"\
	  "    fprintf(stderr, \"%s: there should be at most"\
	  " $cmax non-option argument(s)\\n\",\n"\
	  "            Program);\n"\
	  "    exit(EXIT_FAILURE);\n"\
	  "  }\n"
    }

  } else {
    ##### no Rest-command
    append res \
	"  if( cmd.argc>0 ) {\n"\
	"    fprintf(stderr, \"%s: There are %d arguments not "\
	"associated with any option\\n\",\n"\
	"            Program, cmd.argc);\n"\
        "    exit(EXIT_FAILURE);\n"\
	"  }\n"
  }

  append res \
      "  /*@-compmempass*/"\
      "  return &cmd;\n"\
      "\}\n"
  return $res
}
