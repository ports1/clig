#! /usr/bin/wish8.0

#############################################################################
#
#
# interface for command-line programs
#
# $Revision: 1.1 $, $Date: 1998/08/26 08:44:49 $
#
############################################################################

set base |BASE|
# If this software was not (yet) installed (correctly), the environment
# variable CLIG should point to the files to source in. As a last
# resort, a fixed path in my home directory is choosen.
if {"$base"=="|BASE|"} then {
    if [info exist env(CLIG)] then {
	set base $env(CLIG)
    } else {
	set base [glob ~/demux/]
    }
}

# search all the arrays needed for each program-line created
set programs_list ""
foreach match [glob var.*] {
    source $match
    set pgr [file extension $match]
    regexp {[^.].*} $pgr pgr
    lappend programs_list $pgr
}

# source used in the other program for balloon help
source /home1/sandrine/demux/balloonHelp.tcl
# source used in the other program for tests on entries
source /home1/sandrine/util.tcl

#set ober_info 1
#set argv "  \" \" "
#source /home1/sandrine/demux/globalpg.tcl


# global program
namespace eval ober {

    frame .ober
    pack .ober

    frame .ober.titles
    label .ober.titles.progname -text "program" -width 10
    label .ober.titles.cmdline -text "command line" -width 50
    pack .ober.titles.progname -anchor c -side left -padx 5
    pack .ober.titles.cmdline -anchor c -side left
    
    pack .ober.titles -fill x
    
    frame .ober.buttons
    button .ober.buttons.cancel -text Cancel -command exit
    
    # line program fields
    foreach program $programs_list {
	frame .ober.line$program -borderwidth 2 -relief groove 
	button .ober.line$program.pgbutton -text $program \
	    -command "ober::commande $program" -width 8
	label .ober.line$program.cmdline \
	    -text "$program [set ${program}::Lbis]"
	button .ober.line$program.runbutton -text RUN \
	    -command "[namespace current]::${program}::create_finalstring run"\
	    -state disabled
	
	pack .ober.line$program.pgbutton .ober.line$program.cmdline -side left 
	pack .ober.line$program.runbutton -side right
	pack .ober.line$program -pady 2 -fill x
    }
    
    # proc called by a click on a button to create an interface for the
    # associated program
    proc commande {program} {
	#global ober_info
	#unset ober_info
	set argv "$program ober"
	source /home1/sandrine/demux/globalpg.tcl
	tkwait window .$program 
	#set ober_info 1
	update_ober $program
    }
    
    # procedure that updates the command line written and allows the click
    # on RUN button
    proc update_ober {program} {
	.ober.line$program.cmdline configure \
	    -text [[namespace current]::${program}::create_finalstring exit]
	.ober.line$program.runbutton configure -state normal
    }
    
    # packs 
    wm title . OBER
    pack .ober.buttons.cancel -side bottom
    pack .ober.buttons
}











