#! /usr/bin/wish8.0

##############################################################
#
#
# program to launch the TCL interface
# need the file var.<name> created by clig (through genGUI.tcl)
#
# Olieric Sandrine $Revision: 1.1 $, $Date: 1998/08/26 08:44:50 $
#
#
##############################################################

set base |BASE|
# If this software was not (yet) installed (correctly), the environment
# variable CLIG should point to the files to source in. As a last
# resort, a fixed path in my home directory is choosen.
if {"$base"=="|BASE|"} then {
    if [info exist env(CLIG)] then {
	set base $env(CLIG)
    } else {
	set base [glob ~/demux/]
    }
}


# parameter when called : the name of the program ("$nameString")
# else the window is destroyed before you can see it...

# source for the balloon help
source /home1/sandrine/demux/balloonHelp.tcl

# source for entry verification (number of strings)
source /home1/sandrine/util.tcl

# source for the variables
if {[info exists argv]} {
    #  if {"[lindex $argv 0]"!=" "} {}
    if {"[lindex $argv 0]"!=""} {source /home1/sandrine/demux/var.[lindex $argv 0]
    } else {
	puts "You forgot to choose a program name"
	destroy .
    }
}

# description of the variables
# N contains the name of the program, and is used as name for the window
# and is concatenated with the name of the "global" array TCL

# list of all the global variables used in the program
# that are collected in the array TCL${N}.
# the following indices are used:
#
# nameString : contains the name of the program to be launched through
#              the generated interface
#
# usageString : contains the program's usage
#
# command_string : contains the reconstitution of the command line
#                  for the program to be launched through the interface
#
# MAX : contains the maximum of strings that can be used 
#       for the same option (-o, ...)
#
# TCL${N}($i)
# array containing each option of the program (but "rest") such as -o,...
#
# $type$TCL${N}($i)$k : textvariable
#                      type is one of string, float, int and 
#                      TCL${N}($i) contains the option -o,...contains 
#                      the entry for one string of an option
#
# $type[set TCL${N}($i)] : this contains the concatanation of all the string 
#                    used for an option
#                    if type is flag, this is a variable containing
#                    1 if the option is checked, 0 either.
#
# ${type}Number$i : textvariable, contains the number 
#                   of desired string for an option
#
######################################################

namespace eval [lindex $argv 0] {

    # procedure that create the associated string for the program launched
    # type_of_call is either exit or run.
    proc create_finalstring {type_of_call} {
	variable Dbis
	variable Lbis 
	variable N
	variable TCL${N}
	
	set TCL${N}(command_string) "$N "
	# "rest" field first because some programs can't see the difference 
	# between the last option and "rest" that doesn't begin with "-"
	if  {"[lsearch $Dbis ...,typename*]"!="-1"} {
	    set type [lindex [lindex $Dbis [lsearch $Dbis ...,typename*]] 1]
	    set  TCL${N}($type[set TCL${N}([llength $Lbis])]) ""
	    set k 1
	    while {[info exists TCL${N}($type[set TCL${N}([llength $Lbis])]${k})]} {
		append TCL${N}($type[set TCL${N}([llength $Lbis])]) \
		    "[set TCL${N}($type[set TCL${N}([llength $Lbis])]${k})] "
		incr k 1
	    }
	    append TCL${N}(command_string) \
		"[set TCL${N}($type[set TCL${N}([llength $Lbis])])] "
	}
	# all the other fields
	for {set i 0} {$i<[llength $Lbis]} {incr i 1} {
	    # List_index contains the list with -..,typename as first part
	    set List_index_typename \
		[lindex $Dbis [lsearch $Dbis [lindex $Lbis $i],typename*]]
	    # type contains flag , string , ....
	    set type [lindex $List_index_typename 1]
	    set k 1
	    # create a string with all the entries for the same option -..
	    if {"$type"!="flag"} {
		set  TCL${N}($type[set TCL${N}($i)]) ""
		if {[info exists TCL${N}($i)]} {
		    # for the fields that exist
		    while {[info exists TCL${N}($type[set TCL${N}($i)]${k})] &&\
			       $k<=[set TCL${N}(${type}Number$i)] } {
			# if the field is not empty
			if {[set TCL${N}($type[set TCL${N}($i)]${k})]!=""} {
			    append TCL${N}($type[set TCL${N}($i)]) \
				"[set TCL${N}($type[set TCL${N}($i)]${k})] "
			}
			incr k 1
		    }
		    # if the entry is not empty
		    if {[set TCL${N}($type[set TCL${N}($i)])]!=" "} {
			append TCL${N}(command_string) \
			    "[set TCL${N}($i)] [set TCL${N}($type[set TCL${N}($i)])]"
		    }
		} elseif {"[set TCL${N}($type[set TCL${N}($i)])]"=="1"} {
		    # only when flag button checked !
		    append TCL${N}(command_string) "[set TCL${N}($i)] "  
		}
	    }
	}    
	puts [set TCL${N}(command_string)]
	if {"$type_of_call"=="run"} {
	eval exec [set TCL${N}(command_string)]
	}
	return  [set TCL${N}(command_string)]
	
    }

    # procedure that destroy all the entries of the $i string field
    proc destroyentries {i type} {
	variable N
	variable TCL${N} 
	for {set k 1} {$k <= [set TCL${N}(MAX)]} {incr k} {
	    if {[winfo exists .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice$k]} {
		destroy .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice$k
	    }
	}
    }
    
    # proc to quit properly the program in the 2 cases : 
    # when called as a stand-alone one or when called by another one
    proc exit_properly {} {
	variable N
	if {"[namespace current]"=="::$N"} {
	    destroy .
	} else {
	    destroy .$N
	}
    }
    
    # procedure that create the number of entries that is entered 
    # by user for a particular option (type string)
    proc entries_number {i cmin cmax type} {
	variable N
	variable TCL${N}
	set NumberError [test_with_dialog .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice $cmin $cmax integer]
	# cut the number of entries to TCL${N}(MAX)
	if {$NumberError ==0} { 
	    set NumberError [test_with_dialog .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice $cmin [set TCL${N}(MAX)] integer]}
	if {$NumberError ==0} {
	    destroyentries $i $type
	    for {set k 1} {$k<=[set TCL${N}(${type}Number$i)]} {incr k 1} { 
		entry .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice$k -textvariable [namespace current]::TCL${N}($type[set TCL${N}($i)]${k})
		pack .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice$k -side top
	    }
	    .$N.field[set TCL${N}($i)] configure \
		-relief groove -borderwidth 2
	} else {}
    }
    
    # create the option field for each option (but flag) with 
    # all the entries
    proc create_optionfield {i cmin cmax rangemin rangemax Default type} {
	variable N
	variable TCL${N} 
	label .$N.field[set TCL${N}($i)].option.label \
	    -text [set TCL${N}($i)] -anchor e -width 8
	frame .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)]
	# if the user can use different entries...
	if {$cmax >1} {
	    label .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choicelabel -text "Number of $type:"
	    entry .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice \
		-width 5 \
		-textvariable [namespace current]::TCL${N}(${type}Number$i)
	    bind .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice \
		<Return> \
		"[namespace current]::entries_number $i $cmin $cmax $type"
	    pack .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choicelabel\
		.$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice \
		-side left
	} else {
	    set TCL${N}(${type}Number$i) 1
	    entry .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice1 \
		-textvariable [namespace current]::TCL${N}($type[set TCL${N}($i)]1)
	    set TCL${N}($type[set TCL${N}($i)]1) $Default
	    pack .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)].choice1 \
		-side left
	}
	# range
	if {$type!="string"} {
	label .$N.field[set TCL${N}($i)].type \
	    -text "($type range=$rangemin,$rangemax)" -fg grey50
	} else {
	label .$N.field[set TCL${N}($i)].type \
	    -text ($type) -fg grey50
	}
	pack .$N.field[set TCL${N}($i)].option.label \
	    .$N.field[set TCL${N}($i)].nameLabel \
	    .$N.field[set TCL${N}($i)].$type[set TCL${N}($i)] \
	    .$N.field[set TCL${N}($i)].type -side left
    }
   
    ################### main program ######################
    #######################################################
    proc main {argv} {
	
	variable Lbis
	variable Dbis
	variable N
	variable TCL${N}

	# set of the maximum of strings for an option
	set TCL${N}(MAX) 50
	
	# creates the global frame, the titles,... and packs them
	if {[lindex $argv 1]!="ober"} {	
	    frame .$N
	    pack .$N -fill x
	} else {
	    toplevel .$N
	}

	frame .$N.title
	label .$N.title.label -text $N  -justify center
	pack .$N.title.label
	
	frame .$N.description
	label .$N.description.pg_description \
	    -text [set TCL${N}(usageString)]
	pack .$N.description.pg_description
	
	frame .$N.button
	button .$N.button.run -text RUN  \
	    -command "[namespace current]::create_finalstring run"
	button .$N.button.exit -text EXIT \
	    -command "[namespace current]::exit_properly"
	pack .$N.button.run .$N.button.exit  -side left
	pack .$N.button -side bottom
	
	pack .$N.title -fill x
	pack .$N.description -side top
	
	
	# global loop for i in the option list L
	for {set i 0} {$i<[llength $Lbis]} {incr i 1} {
	    # TCL${N}($i) contains  -<word>
	    set TCL${N}($i) [lindex $Lbis $i]
	    # all the list that followed are crushed by the next ones
	    # List_index contains the list with -<word>,typename as first part
	    set List_index_typename [lindex $Dbis [lsearch $Dbis [lindex $Lbis $i],typename*]]
	    set List_index_name [lindex $Dbis  [lsearch $Dbis \
						    [lindex $Lbis $i],name*]]
	    # the List_index_usage contains the index of list 
	    # usage for each option
	    set List_index_usage [lindex $Dbis [lsearch $Dbis \
						    [lindex $Lbis $i],usage*]]
	    # usage is crushed with the next option usage.
	    set usage ""
	    for {set j 1} {$j<[llength $List_index_usage]} {incr j 1} {
		append usage  [lindex $List_index_usage $j ] { }
	    }
	    # type contains flag , string , ....
	    set type [lindex $List_index_typename 1]
	    # List_index_default contains the index of the list containing 
	    # the default value. Default contains the value itself
	    set List_index_default [lindex $Dbis  [lsearch $Dbis [lindex $Lbis $i],default*]]
	    if {[lsearch $Dbis [lindex $Lbis $i],default*]=="-1"} {
		set Default ""
	    } else {
		set Default [lindex $List_index_default 1]
	    }
	    # create the 2 limit of string number, if don't exist, set to 0
	    set List_cmin [lindex $Dbis [lsearch $Dbis  [lindex $Lbis $i],cmin*]]
	    if {[lsearch $Dbis [lindex $Lbis $i],cmin*]!="-1"} {
		set cmin [lindex $List_cmin 1]} else {set cmin 0}
	    set List_cmax [lindex $Dbis [lsearch $Dbis  [lindex $Lbis $i],cmax*]]
	    if  {[lsearch $Dbis [lindex $Lbis $i],cmax*]!="-1"} {
		set cmax [lindex $List_cmax 1]} else {set cmax 0}
	    
	    # range
	    set List_min [lindex $Dbis [lsearch $Dbis  [lindex $Lbis $i],min*]]
	    set List_max [lindex $Dbis [lsearch $Dbis  [lindex $Lbis $i],max*]]
	    if {[lsearch $Dbis [lindex $Lbis $i],min*]!="-1"} {
		set rangemin [lindex $List_min 1]
		set rangemax [lindex $List_max 1]
	    }
	    
	    # create a frame for each option
	    frame .$N.field[set TCL${N}($i)]
	    frame .$N.field[set TCL${N}($i)].option -width 10 \
		-relief groove -borderwidth 2  
	    balloonHelp .$N.field[set TCL${N}($i)].option $usage
	    pack .$N.field[set TCL${N}($i)].option -side left
	    label .$N.field[set TCL${N}($i)].nameLabel \
		-text [lindex $List_index_name 1] -width 10 -anchor w
	    
	    # call to the procedure that create the rest of the field
	    switch $type {
		flag {
		    checkbutton .$N.field[set TCL${N}($i)].option.button[set TCL${N}($i)] -variable [namespace current]::TCL${N}($type[set TCL${N}($i)])
		    label .$N.field[set TCL${N}($i)].option.label  \
			-text [set TCL${N}($i)] -anchor e -width 4
		    pack .$N.field[set TCL${N}($i)].option.button[set TCL${N}($i)] \
			.$N.field[set TCL${N}($i)].option.label \
			.$N.field[set TCL${N}($i)].nameLabel -side left
		}
		string {create_optionfield $i $cmin $cmax 0 0 $Default string }
		float {create_optionfield $i $cmin $cmax $rangemin $rangemax $Default float }
		integer {create_optionfield $i $cmin $cmax $rangemin $rangemax $Default integer}
	    }
	    
	    pack .$N.field[set TCL${N}($i)] -fill x
	}
	# "rest" of the string line
	if {"[lsearch $Dbis ...,*]"!="-1"} {
	    set TCL${N}($i) ""
	    set List_index_rest [lindex $Dbis [lsearch $Dbis ...,*]]
	    set cmin [lindex [lindex $Dbis [lsearch $Dbis ...,cmin*]] 1]
	    set cmax [lindex [lindex $Dbis [lsearch $Dbis ...,cmax*]] 1]
	    set type [lindex [lindex $Dbis [lsearch $Dbis ...,type*]] 1]
	    set name [lindex [lindex $Dbis [lsearch $Dbis ...,name*]] 1]
	    set Default [lindex [lindex $Dbis [lsearch $Dbis ...,default*]] 1]
	    set usage ""
	    set List_index_usage  [lindex $Dbis [lsearch $Dbis ...,usage*]]
	    for {set j 1} {$j<[llength $List_index_usage]} {incr j 1} {
		append usage  [lindex $List_index_usage $j ] { }
	    }
	    frame .$N.field[set TCL${N}($i)]
	    frame .$N.field[set TCL${N}($i)].option \
		-width 10  -relief groove -borderwidth 2  
	    balloonHelp .$N.field[set TCL${N}($i)].option $usage
	    pack .$N.field[set TCL${N}($i)].option \
		-side left
	    label .$N.field[set TCL${N}($i)].nameLabel \
		-text  $name -width 10 -anchor w
	    
	    pack .$N.field[set TCL${N}($i)] -fill x
	    # $i is the next value of i at the end of the for loop...
	    create_optionfield $i $cmin $cmax 0 0 $Default $type
	}
	bind all <Control-c> exit  
    }
}

# call to open the program
if {"[lindex $argv 0]"!=""} {
    [lindex $argv 0]::main $argv
}
#if {![info exists ober_info]} {
#    if {"[lindex $argv 0]"!=""} {
#	[lindex $argv 0]::main $argv
#    }
#}






