
## generate the structure to hold command line parameters
## The following variables are generated for an option with name bla:
##   char blaP : will be set to true, if the option shows up.
## Except for type Flag, also the following vars are generated
##   int bla : the option value if cmax==1
## or
##   int *bla : the values, if cmax>1
##   int blaC : number of values, if cmin<cmax
##
## The generated C-code is returned as the function result.
##
proc genStruct {} {
  global D typeMap

  set res "typedef struct s_Cmdline \{\n"
  
  foreach opt $D(opts) {
    if {[string match "--" $opt]} continue
    append res \
	"  /***** $opt: $D($opt,usage) */\n"\
	"  char $D($opt,var)P;\n"
    
    if { $D($opt,type)=="Flag" } continue

    foreach {cmin cmax} $D($opt,count) break
    set ast [expr {$cmax==1 ? "" : "*"}]
    append res \
	"  $typeMap(C,$D($opt,type)) $ast$D($opt,var);\n" \
	"  int $D($opt,var)C;\n"

  }
  append res \
      "  /***** uninterpreted command line parameters */\n"\
      "  int argc;\n" \
      "  /*@null*/char **argv;\n"

  ## Do we generate a copy of the command line as one string?
  if [info exist D(commandline)] {
    append res \
	"  /***** the whole command line concatenated */\n"\
	"  char *$D(commandline);\n"
  }
  append res \
      "\} Cmdline;\n"

  return $res
}


