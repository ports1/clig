########################################################################
#
# genDefault is a bit of a misnomer, because not only the default
# values are generated, but also the `struct Cmdline' returned to
# main().
#
########################################################################

########################################################################
#
# wrap quotes around a string, if the option type is String
#
proc cValue {val type} {
  if {[string match String $type]} {
    return "\"$val\""
  } else {
    return $val
  }
}
########################################################################
#
# Generate a default value. If count<=1 for this option, it is the
# value specified in the description file, quoted if necessary. If
# count can be greater 1, it is the name of a variable holding the
# default values.
#
proc genDefaultVal {opt} {
  global D typeMap

  set res {}

  if {[info exist D($opt,default)]} {
    if { [lindex $D($opt,count) 1]==1 } {
      append res [cValue [lindex $D($opt,default) 0] $D($opt,type)]
    } else {
      ## return a suitable variable name, if we have a default
      append res "$D($opt,var)Default"
    }
  } else {
    set ast [expr {[lindex $D($opt,count) 1]==1 ? "" : "*"}]
    append res "($typeMap(C,$D($opt,type))$ast)0"
  }

  return $res;
}
########################################################################
#
# Generate C-code for default-arrays. These are used for options
# having an argument count greater than 1.
#
proc genDefaultArrays {} {
  global D typeMap

  set res {}

  foreach opt $D(opts) {
    if {[string match "--" $opt]} continue
    if { ![info exist D($opt,default)] } continue
    set cmax [lindex $D($opt,count) 1]
    if { ![Infty $cmax] && $cmax<= 1 } continue

    append res \
	"static $typeMap(C,$D($opt,type)) $D($opt,var)Default\[\] = \{"

    set comma {}
    foreach x $D($opt,default) {
      append res $comma [cValue $x $D($opt,type)]
      set comma ", "
    }
    append res "\};\n"
  }
  return $res
}
########################################################################
#
# generate the static structure a pointer to which is returned by the
# command line parser to main().
#
proc genDefault {} {
  global D

  set res [genDefaultArrays]
  append res "\nstatic Cmdline cmd = \{\n"
  
  set comma {}
  foreach opt $D(opts) {
    if {[string match "--" $opt]} continue

    append res $comma \
	"  /***** $opt: $D($opt,usage) */\n"\
	"  /* $D($opt,var)P = */ "
    if {[info exist D($opt,default)] } {
      append res 1
    } else {
      append res 0
    }
    set comma ",\n"

    if { $D($opt,type)=="Flag" } continue

    append res \
	",\n  /* $D($opt,var) = */ [genDefaultVal $opt]"\
	",\n  /* $D($opt,var)C = */ "
    if { ![info exist D($opt,default)] } {
      append res 0
    } else {
      append res [llength $D($opt,default)]
    }
  }

  append res $comma "  /***** uninterpreted rest of command line */"
  append res "\n  /* argc = */ 0"
  append res ",\n  /* argv = */ (char**)0"

  ## the whole command line concatenated
  if {[info exist D(commandline)]} {
    append res ",\n  /***** the original command line concatenated */"
    append res "\n  /* $D(commandline) = */ NULL"
  }
  append res "\n\};\n"

  
  return $res
}
