########################################################################
#
# generate C-code to check for mandatory options
#
# $Revision: 1.2 $, $Date
#
########################################################################
proc genCheckMandatory {w} {
  global D haveMandatory

  if { !$haveMandatory } {
    return {}
  }

  set res {}
  foreach opt $D(opts) {
    if {[string match "--" $opt]} continue
    if { [info exist D($opt,mandatory)] } {
      append res \
	  "${w}if( !cmd.$D($opt,var)P ) \{\n"\
	  "${w}  missingErr(\"$opt\");\n"\
	  "${w}  missingMandatory = 1;\n"\
	  "${w}\}\n"
    }
  }
  append res \
      "${w}if( missingMandatory ) exit(EXIT_FAILURE);\n"

  return $res
}
