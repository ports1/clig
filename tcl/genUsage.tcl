########################################################################
##
## generate count description for usage-function, i.e. s.th. like
##      3...6 integer values
## or
##      1 or more string values
##
proc usageCount {opt} {
  global D typeMap

  foreach {cmin cmax} $D($opt,count) break

  append usage $cmin
  if {[Infty $cmax]} {
    append usage " or more"
  } elseif { $cmax>$cmin } {
    append usage "...$cmax"
  }

  if {"$opt"!="--"} {
    append usage " $typeMap(C,$D($opt,type))"
  }

  if { $cmax==1 && $cmin==1} {
    append usage " value"
  } else {
    append usage " values"
  }	
}
########################################################################
proc genUsage {} {
  global D

  #####
  ##### generate synopsis
  #####
  set usage {}
  foreach opt $D(opts) {
    if {[string match "--" $opt]} continue

    set optional [expr {![info exist D($opt,mandatory)]}]

    if {$optional} { 
      append usage " \[$opt" 
    } else {
       append usage " $opt" 
    }

    if { $D($opt,type)!="Flag" } {
      if {0==[lindex $D($opt,count) 0]} { 
	append usage " \[$D($opt,var)\]" 
      } else { 
	append usage " $D($opt,var)" 
      }
    }

    if $optional { append usage "\]" }
  }

  if { [info exist D(--,type)] } {
    append usage " \[--\] "
    foreach {cmin cmax} $D(--,count) break
    if { $cmin==0 } {append usage "\["}
    append usage "$D(--,var)"
    if { [Infty $cmax] || $cmax>1 }  {append usage " ..."}
    if { $cmin==0 } {append usage "\]"}
  }
  regsub -all "\n" $D(usage) "\\n\n    " s
  append usage "\\n\n"
  append usage "    $s\\n\n"

  #####
  ##### find maximum length of options
  #####
  if { [info exist D(--,var)] } {
    set max [string length $D(--,var)]
  } else {
    set max 0
  }
  foreach opt $D(opts) {
    set l [string length $opt]
    if { $l>$max } {set max $l}
  }
  incr max 2

  #####
  ##### description of every option
  #####
  foreach opt $D(opts) {
    if {[string match "--" $opt]} continue

    set l [string length $opt]
    append usage [b [expr $max-$l]]
    set tab [b [expr $max+2]]
    regsub -all "\n" $D($opt,usage) "\\n\n$tab" s
    append usage "$opt: $s\\n\n"

    if { $D($opt,type)=="Flag" } continue

    ##### count
    append usage [b [expr $max+2]] [usageCount $opt]
    
    ##### range
    if {[info exist D($opt,range)]} {
      foreach {rmin rmax} $D($opt,range) break
      if {!([negInfty $rmin] && [Infty $rmax])} {
	append usage " between $rmin and $rmax"
      }
    }

    ##### default:
    if {[info exist D($opt,default)]} {
      append usage "\\n\n[b $max]  default:"
      foreach x $D($opt,default) {
	append usage " `$x'"
      }
    }
    append usage "\\n\n"
  }
  #####
  ##### description of non-option arguments
  #####
  if {[info exist D(--,var)]} {
    set l [string length $D(--,var)]
    append usage [b [expr $max-$l]]
    set tab [b [expr $max+2]]
    regsub -all "\n" $D(--,usage) "\\n\n$tab" s

    append usage "$D(--,var): $s\\n\n"
    append usage [b [expr $max+2]] [usageCount --] "\\n\n"
  }

  #####
  ##### version string
  #####
  if {[info exist D(version)]} {
    append usage "version: $D(version)\\n\n"
  }

  set res {}
  append res "void\n" \
      "usage(void)\n" \
      "{\n"
  foreach line [split $usage \n] {
	append res "  fprintf(stderr,\"%s\",\"  $line\");\n"
  }

  append res "  exit(EXIT_FAILURE);\n" \
      "}\n" \
      "/***********************************" \
      "***********************************/"
  return $res
}
########################################################################
