########################################################################
#
# Generate a debugging function to be called by main which prints all
# options found on the command line.
#
########################################################################
proc fformat {typ} {
  switch $typ {
    String {return %s}
    Int {return %d}
    Long {return %ld}
    Float {return %.40g}
    Double {return %.40g}
    default {Autsch}
  }
}
########################################################################
proc genShowOptionValues {} {
  global D

  set res {}
  append res \
      "void\nshowOptionValues(void)\n\{\n"\
      "  int i;\n"

  if [info exist D(commandline)] {
    append res \
	"\n"\
	"  printf(\"Full command line is:\\n`%s'\\n\","\
	" cmd.$D(commandline));\n"
  }

  foreach opt $D(opts) {
    if {[string match "--" $opt]} continue
    ##### print flag value
    append res \
	"\n"\
	"  /***** $opt: $D($opt,usage) */\n"\
	"  if( !cmd.$D($opt,var)P ) \{\n"\
	"    printf(\"$opt not found.\\n\");\n"\
	"  \} else \{\n"\
	"    printf(\"$opt found:\\n\");\n"
    
    if { $D($opt,type)=="Flag" } {
      append res "  \}\n"
      continue
    }
    
    ##### test of count
    append res \
	"    if( !cmd.$D($opt,var)C ) \{\n"\
	"      printf(\"  no values\\n\");\n"

    foreach {cmin cmax} $D($opt,count) break
    if { $cmax==1 } {
      ##### print single value
      append res \
	  "    \} else \{\n"\
	  "      printf(\"  value = `[fformat $D($opt,type)]'\\n\", "\
                                     	  "cmd.$D($opt,var));\n"\
	  "    \}\n"
    } else {
      ##### print multiple values
      append res \
	  "    \} else \{\n"\
	  "      printf(\"  values =\");\n"\
	  "      for(i=0; i<cmd.$D($opt,var)C; i++) \{\n"\
	  "        printf(\" `[fformat $D($opt,type)]'\", "\
	                                  "cmd.$D($opt,var)\[i\]);\n"\
	  "      \}\n"\
	  "      printf(\"\\n\");\n"\
	  "    \}\n"
    }
    append res \
	"  \}\n"
  }
  ##### take care of argc and argv
  append res \
      "  if( !cmd.argc ) \{\n"\
      "    printf(\"no remaining parameters in argv\\n\");\n"\
      "  \} else \{\n"\
      "    printf(\"argv =\");\n"\
      "    for(i=0; i<cmd.argc; i++) \{\n"\
      "      printf(\" `%s'\", cmd.argv\[i\]);\n"\
      "    \}\n"\
      "    printf(\"\\n\");\n"\
      "  \}\n"

  append res \
      "\}\n"\
      "/***********************************"\
      "***********************************/\n"
}
