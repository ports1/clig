
########################################################################
## a local global value
set newMan {}		;# file handle of manual output file


########################################################################

proc o {s} {
  global newMan
  puts -nonewline $newMan $s
}
########################################################################
proc on {s} {
  global newMan
  puts $newMan $s
}
########################################################################
proc skipToEnd {inFile tag} {
  global Program newMan
  set rexp {\.\\\" +cligPart +}
  append rexp "$tag +end"
  while { -1<[gets $inFile line] } {
    if {[regexp $rexp $line]} return
  }
  return -code error 0
}
########################################################################
proc doManTITLE {} {
  global D

  if { [info exist D(version)] } {
    set v $D(version)
  } else {
    set v [clock format [clock seconds] -format %Y-%m-%d]
  }
  o ".TH \"$D(name)\" 1 \"$v\" "
  on "\"Clig-manuals\" \"Programmer's Manual\""
}
########################################################################
proc doManNAME {} {
  global D

  on ".SH NAME"
  on "$D(name) \\- $D(usage)"
}
########################################################################
proc doManSYNOPSIS {} {
  global D L nameString
  on ".SH SYNOPSIS"
  on ".B $D(name)"

  #####
  ##### generate an entry for every option
  #####
  foreach opt $D(opts) {
    if {[string match "--" $opt]} continue

    set optional [expr ![info exist D($opt,mandatory)]]

    if $optional {o "\["}
    o $opt
    
    if { $D($opt,type)!="Flag" } {
      set parOptional [expr {0==[lindex $D($opt,count) 0]}]
      if {$parOptional} { 
	o " \[" 
      } else { 
	o " " 
      }
      o $D($opt,var)
      
      if {$parOptional} { o "\]" }
    }
    if {$optional} {o "\]"}
    on {}
  }
  #####
  ##### generate dots, if non-option parameters are allowed, i.e. Rest
  #####
  if { [info exist D(--,type)] } {
    foreach {cmin cmax} $D(--,count) break
    if { $cmin==0 } {o "\["}
    o "$D(--,var)"
    if { [Infty $cmax] || $cmax>1 }  {o " ..."}
    if { $cmin==0 } {o "\]"}
  }
  on {}
}
########################################################################
proc doManOPTIONS {} {
  global D L

  on ".SH OPTIONS"

  ##### description of every parameter
  foreach opt $D(opts) {
    if {[string match "--" $opt]} continue

    if { $D($opt,type)=="Flag" } {
      on ".IP $opt\n$D($opt,usage)."
    } else {
      on ".IP $opt\n$D($opt,usage),"
      ##### count
      foreach {cmin cmax} $D($opt,count) break
      o ".br\n$cmin"
      if { [Infty $cmax] } {
	o " or more"
      } elseif { $cmax>$cmin } {
	o "...$cmax"
      }
      if {$cmax==1 && $cmin==1} {
	o " $D($opt,type) value"
      } else {
	o " $D($opt,type) values"
      }	
      
      ##### range
      if {[info exist D($opt,range)]} {
	foreach {rmin rmax} $D($opt,range) break
	if {!([negInfty $rmin] && [Infty $rmax])} {
	  o " between $rmin and $rmax"
	}
	o .
      }
      ##### default:
      if {[info exist D($opt,default)]} {
	o "\n.br\nDefault:"
	foreach x $D($opt,default) {
	  o " `$x'"
	}
      }
      on {}
    }
  }

  #####
  ##### description of non-option arguments
  #####
  if { [info exist D(--,type)] } {
    on ".IP $D(--,var)"
    on $D(--,usage).
  }
}  
########################################################################
proc doManDESCRIPTION {} {
  global D

  on ".SH DESCRIPTION"
  if { [info exist D(description)] } {
    on $D(description)
  } else {
    on "This manual page was generated automagically by clig, the"
    on "Command Line Interface Generator. Actually the programmer"
    on "using clig was supposed to edit this part of the manual"
    on "page after"
    on "generating it with clig, but obviously (s)he didn't."
    on "\nSadly enough clig does not yet have the power to pick a good"
    on "program description out of blue air ;-("
  }
}
########################################################################
proc genMan {manFile} {
  global D BASE Program
  global newMan

  ##### if the manual file does not exist, get a template file
  if {![file exist $manFile]} {
    file copy [file join $BASE c template.man] $manFile
  }
  
  ##### open the input file
  set oldMan [open $manFile r]

  ##### open a temporary file
  #puts " opening $manFile.[pid]"
  set newMan [open $manFile.[pid] w]

  #####
  ##### copy the original file into the new manual file, while replacing
  ##### marked sections with new information
  #####
  set hadOneSection 0
  while { -1<[gets $oldMan line] } {
    if {[regexp {.* cligPart ([A-Za-z_0-9]+)} $line dummy section]} {
      set hadOneSection 1
      if {[catch {skipToEnd $oldMan $section}]} {
	set msg {}
	append msg \
	    "$Program: did not find end of `cligPart $section' " \
	    "in file `$manFile'"
	puts stderr $msg
	close $oldMan
	close $newMan
	file delete -force $manFile.[pid]
	exit 1
      }
      on ".\\\" cligPart $section"
      doMan$section
      on ".\\\" cligPart $section end"
    } else {
      on $line
    }
  }

  close $oldMan
  close $newMan
  file rename -force $manFile.[pid] $manFile

  #####
  ##### Warning, if the man-page had nothing to edit
  #####
  if { ! $hadOneSection } {
    set msg {}; append msg \
	"$Program\(warning): template `$manFile' " \
	"has no section to edit"
    puts stderr $msg
  }
}
