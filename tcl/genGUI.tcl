########################################################################
#
# generate the .tcl-file...
#
#  july 1998
#    
# $Revision: 1.2 $, $Date: 1999/05/08 09:44:32 $
########################################################################
proc genGUI {outName} {
    # global cbase debug D L Program cligVersion haveMandatory
    global D L nameString usageString
    
    #  set pname [file tail [file rootname $Program]]
    #  set outBase [file tail [file rootname $outName]]
    set out [open $outName w]

    set pgr "
source balloonHelp.tcl
source util.tcl
frame .$nameString
pack .$nameString -fill x
frame .$nameString.title
label .$nameString.title.label -text $nameString -justify center
pack .$nameString.title.label


frame .$nameString.description
label .$nameString.description.pg_description -text {$usageString}
pack .$nameString.description.pg_description

frame .$nameString.button
button .$nameString.button.ok -text OK -command exit
pack .$nameString.button.ok 
pack .$nameString.button -side bottom

pack .$nameString.title -fill x
pack .$nameString.description -side top
  proc destroyentries {max} {
	for {set i 1} {\$i <= \$max} {incr i} {
	    if {\[winfo exists  .$nameString.field\$Option(\$i).string\$Option(\$i).choice\$k\]} then {
		destroy .$nameString.field\$Option(\$i).string\$Option(\$i).choice\$k
	    }
	}
    }
proc string_number {i cmin cmax} \{
   global Option
   set NumberError \[test_with_dialog .$nameString.field\$Option(\$i).string\$Option(\$i).choice \$cmin \$cmax integer\]
   if \{\$NumberError ==0\} \{
      for \{set k 1\} \{\$k<=\$Option(StringNumber\$i)\} \{incr k 1\} \{ 
          entry .$nameString.field\$Option(\$i).string\$Option(\$i).choice\$k
          pack .$nameString.field\$Option(\$i).string\$Option(\$i).choice\$k \
               -side bottom
    
      \}
   .$nameString.field\$Option(\$i) configure -relief groove -borderwidth 2
\} else \{\}
\}
"

   append pgr "set Lbis [list $L] 
"

#### HP **************
set Dlist ""
foreach index [array names D] {
    lappend  Dlist "$index $D($index)"
}
set Dlist [lsort -ascii $Dlist]
set Dbis [list $Dlist]
set Lbis [list $L]
# on a Dbis et Lbis pour bosser (doublons).

######## Scripte ############
append pgr "set Dbis [list $Dlist]
puts \$Dbis
"

append pgr "
for \{set i 0\} \{\$i<\[llength $Lbis\]\} \{incr i 1\} \{
        # Option contains  -<word>
	set Option(\$i) \[lindex $Lbis \$i\]
        # List_index contains the list with -<word>,typename as first part
        set List_index_typename \[lindex $Dbis \[lsearch $Dbis \[lindex $Lbis \$i\],typename*\]\]
        set List_index_name \[lindex $Dbis \[lsearch $Dbis \[lindex $Lbis \$i\],name*\]\]
        # the List_index_usage contains the usages 
        set List_index_usage \[lindex $Dbis \[lsearch $Dbis \
                                  \[lindex $Lbis \$i\],usage*\]\]
        set usage {}
        for \{set j 1\} \{\$j<\[llength \$List_index_usage\]\} \{incr j 1\} \{
                append usage  \[lindex \$List_index_usage \$j \] { }
        \}
        # type contains flag , string , ....
	set type \[lindex \$List_index_typename 1\]
"

append pgr "
        # create a frame for each option
        frame .$nameString.field\$Option(\$i)
        frame .$nameString.field\$Option(\$i).left -width 10 \
              -relief groove -borderwidth 2  
        balloonHelp .$nameString.field\$Option(\$i).left \$usage
        pack .$nameString.field\$Option(\$i).left -side left
        label .$nameString.field\$Option(\$i).nameLabel \
                -text \[lindex \$List_index_name 1 \] -width 10 -anchor w
"

append pgr "
	switch \$type \{
	    flag \{ 
		checkbutton .$nameString.field\$Option(\$i).left.button\$Option(\$i)
                label .$nameString.field\$Option(\$i).left.label \
                      -text \$Option(\$i) -anchor e -width 4
		pack .$nameString.field\$Option(\$i).left.button\$Option(\$i) \
                     .$nameString.field\$Option(\$i).left.label \
                     .$nameString.field\$Option(\$i).nameLabel -side left
	    \}
	    string \{ 
                label .$nameString.field\$Option(\$i).left.label \
                       -text \$Option(\$i) -anchor e -width 8
                set List_cmin \[lindex $Dbis \[lsearch $Dbis \
                                  \[lindex $Lbis \$i\],cmin*\]\]
                set cmin \[lindex \$List_cmin 1\]
                set List_cmax \[lindex $Dbis \[lsearch $Dbis \
                                  \[lindex $Lbis \$i\],cmax*\]\]
                set cmax \[lindex \$List_cmax 1\]
                frame .$nameString.field\$Option(\$i).string\$Option(\$i)
                label .$nameString.field\$Option(\$i).string\$Option(\$i).choicelabel -text \"Number of string\"
                entry .$nameString.field\$Option(\$i).string\$Option(\$i).choice -width 5 -textvariable Option(StringNumber\$i)
                bind .$nameString.field\$Option(\$i).string\$Option(\$i).choice <Return> \"string_number \$i \$cmin \$cmax \"
                pack  .$nameString.field\$Option(\$i).string\$Option(\$i).choicelabel .$nameString.field\$Option(\$i).string\$Option(\$i).choice -side left
                label .$nameString.field\$Option(\$i).type \
                      -text (\$type) -fg grey50
                pack .$nameString.field\$Option(\$i).left.label \
                     .$nameString.field\$Option(\$i).nameLabel \
                     .$nameString.field\$Option(\$i).string\$Option(\$i) \
                     .$nameString.field\$Option(\$i).type -side left
            \}
	    float \{ 
               label .$nameString.field\$Option(\$i).left.label \
                     -text \$Option(\$i) -anchor e -width 8
               entry .$nameString.field\$Option(\$i).float\$Option(\$i)
               label .$nameString.field\$Option(\$i).type \
                     -text (\$type) -fg grey50
               pack .$nameString.field\$Option(\$i).left.label  \
                    .$nameString.field\$Option(\$i).nameLabel \
                    .$nameString.field\$Option(\$i).float\$Option(\$i) \
                    .$nameString.field\$Option(\$i).type -side left
            \}
	    integer \{ 
               label .$nameString.field\$Option(\$i).left.label \
                     -text \$Option(\$i) -anchor e -width 8
               entry .$nameString.field\$Option(\$i).int\$Option(\$i)
               label .$nameString.field\$Option(\$i).type \
                     -text (\$type) -fg grey50
               pack .$nameString.field\$Option(\$i).left.label  \
                    .$nameString.field\$Option(\$i).nameLabel \
                    .$nameString.field\$Option(\$i).int\$Option(\$i) \
                    .$nameString.field\$Option(\$i).type -side left
            \}
	\}
        pack .$nameString.field\$Option(\$i) -fill x

        bind all <Control-c> exit
   \}
"

    puts $out $pgr
    flush $out
}
