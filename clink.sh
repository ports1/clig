#!/bin/sh -e
#
# usage: clink.sh dstDir file [file ...]
#
# For each file create a soft link in dstDir pointing to it. If file
# exists already in dstDir as a soft link, the link is replaced. If
# file already exist as anything else, this is silently ignored.
# If dstDir does not exist, this is silently ignored
#set -x

if [ $# -lt 2 ]; then exit 1 ; fi

dstDir=$1 ; shift

if [ ! -d $dstDir ]; then
  echo "warning: no soft link(s) created in $dstDir (dir does not exist)"
  exit 0
fi


for file do
  x=`basename $file`

  ## We feel free to remove soft links. Too bad that csh cannot test for
  ## soft links. 
  if [ -h $dstDir/$x ]; then rm $dstDir/$x ; fi

  ## However we don't touch real files
  if ln -s $file $dstDir 2> /dev/null ; then
    echo "creating soft link $dstDir/$x --> $file"
  else
    echo "warning: didn't create soft link $dstDir/$x"
  fi
done
exit 0
##### end #####
