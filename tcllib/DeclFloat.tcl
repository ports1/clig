########################################################################
#
# Declare a command line option with arguments of type float.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.4 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

## make sure, declOpt.tcl can be found
set auto_index(::clig::declOpt) \
    [list source [file join [file dir [info script]] declOpt.tcl]]

namespace eval ::clig {
  namespace export Float

  variable FloatSpec

  set FloatSpec(usage) {declare an option with parameters of type float}

  ## Float has fixed params
  set FloatSpec(fixed) {
    opt {option string to define, like -bla}
    var {name of variable which shall hold the parameters given to -bla}
    usage {descriptive text for -bla}
  }

  set FloatSpec(opts) {-c -d -m -r}
  ## Float has option -c
  set FloatSpec(-c,type) Int
  set FloatSpec(-c,var) count
  set FloatSpec(-c,count) {2 2}
  set FloatSpec(-c,range) {0 oo}
  set FloatSpec(-c,default) {1 1}
  set FloatSpec(-c,usage) \
      {minimum and maximum number of arguments allowed for this option}
  
  ## Float has option -d
  set FloatSpec(-d,type) String
  set FloatSpec(-d,var) default
  set FloatSpec(-d,count) {0 oo}
  set FloatSpec(-d,usage) \
      {default(s) to substitute if option is not given} 

  ## Float has option -m
  set FloatSpec(-m,type) Flag
  set FloatSpec(-m,var) mandatory
  set FloatSpec(-m,usage) \
      {request the defined "option" to be in fact mandatory}

  ## Float has option -r
  set FloatSpec(-r,type) Float
  set FloatSpec(-r,var) range
  set FloatSpec(-r,count) {2 2}
  set FloatSpec(-r,range) {-oo oo}
  set FloatSpec(-r,default) {-oo oo}
  set FloatSpec(-r,usage) \
      {minimum and maximum value of arguments allowed for this option} 
  
  ## usage string
  set FloatSpec(usage) {declare an option with args of type float}
}
########################################################################
proc ::clig::Float  {opt var usage args} {  
  if {[catch {declOpt Float $opt $var $usage $args} err]} {
    return -code error $err
  }
}
