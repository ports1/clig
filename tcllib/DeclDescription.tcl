########################################################################
#
# Enter the description of a program.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.3 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

namespace eval ::clig {
  namespace export Description

  variable DescriptionSpec

  set DescriptionSpec(fixed) {
    description {the description of the declared program/function}
  }
  set DescriptionSpec(usage) {
    enters the description of the declared program/function into its spec
  }
}
########################################################################
proc ::clig::Description {description} {
  variable currentSpec
  upvar $currentSpec spec

  set spec(description) $description
}
########################################################################
