########################################################################
#
# Declare a command line option with arguments of type double.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.3 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

## make sure, declOpt.tcl can be found
set auto_index(::clig::declOpt) \
    [list source [file join [file dir [info script]] declOpt.tcl]]

namespace eval ::clig {
  namespace export Double

  variable DoubleSpec

  set DoubleSpec(usage) {declare an option with parameters of type double}

  ## Double has fixed params
  set DoubleSpec(fixed) {
    opt {option string to define, like -bla}
    var {name of variable which shall hold the parameters given to -bla}
    usage {descriptive text for -bla}
  }

  set DoubleSpec(opts) {-c -d -m -r}
  ## Double has option -c
  set DoubleSpec(-c,type) Int
  set DoubleSpec(-c,var) count
  set DoubleSpec(-c,count) {2 2}
  set DoubleSpec(-c,range) {0 oo}
  set DoubleSpec(-c,default) {1 1}
  set DoubleSpec(-c,usage) \
      {minimum and maximum number of arguments allowed for this option}
  
  ## Double has option -d
  set DoubleSpec(-d,type) String
  set DoubleSpec(-d,var) default
  set DoubleSpec(-d,count) {0 oo}
  set DoubleSpec(-d,usage) \
      {default(s) to substitute if option is not given} 

  ## Double has option -m
  set DoubleSpec(-m,type) Flag
  set DoubleSpec(-m,var) mandatory
  set DoubleSpec(-m,usage) \
      {request the defined "option" to be in fact mandatory}

  ## Double has option -r
  set DoubleSpec(-r,type) Double
  set DoubleSpec(-r,var) range
  set DoubleSpec(-r,count) {2 2}
  set DoubleSpec(-r,range) {-oo oo}
  set DoubleSpec(-r,default) {-oo oo}
  set DoubleSpec(-r,usage) \
      {minimum and maximum value of arguments allowed for this option} 
  
  ## usage string
  set DoubleSpec(usage) {declare an option with args of type double}
}
########################################################################
proc ::clig::Double  {opt var usage args} {  
  if {[catch {declOpt Double $opt $var $usage $args} err]} {
    return -code error $err
  }
}
