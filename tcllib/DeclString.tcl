########################################################################
#
# Declare a command line option with arguments of type string
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.3 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

## make sure, declOpt.tcl can be found
set auto_index(::clig::declOpt) \
    [list source [file join [file dir [info script]] declOpt.tcl]]

namespace eval ::clig {
  namespace export String
  
  variable StringSpec

  ## String has fixed parameters
  set StringSpec(fixed) {
    opt {option string to define, like -bla}
    var {name of variable which shall hold the parameters given to -bla}
    usage {descriptive text for -bla}
  }

  set StringSpec(opts) {-c -d -m}

  ## String has option -c
  set StringSpec(-c,type) Int
  set StringSpec(-c,var) count
  set StringSpec(-c,count) {2 2}
  set StringSpec(-c,range) {0 oo}
  set StringSpec(-c,default) {1 1}
  set StringSpec(-c,usage) {
    minimum and maximum number of arguments allowed for this option
  }

  ## String has option -d
  set StringSpec(-d,type) String
  set StringSpec(-d,var) default
  set StringSpec(-d,count) {0 oo}
  set StringSpec(-d,usage) \
      {default(s) to substitute if option is not given} 

  ## String has option -m
  set StringSpec(-m,type) Flag
  set StringSpec(-m,var) mandatory
  set StringSpec(-m,usage) \
      {request the defined "option" to be in fact mandatory}

  ## usage string
  set StringSpec(usage) {declare an option with args of type string}

      
}
########################################################################
proc ::clig::String  {opt var usage args} {  
  if {[catch {declOpt String $opt $var $usage $args} err]} {
    return -code error $err 
  }
}
########################################################################
