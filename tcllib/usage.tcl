########################################################################
#
# Generate a usage message.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.4 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

namespace eval ::clig {}

########################################################################
proc ::clig::makeOptUsage {min indent name text} {
  set s [string trim $text "\n \t"]
  regsub -all "\n *" $s $indent s
  append msg [format "\n  %${min}s: " $name] $s
}
########################################################################
proc ::clig::makeOptCount {indent type count} {
  foreach {cmin cmax} $count break
  if {$cmin==$cmax} {
    if {1==$cmin} {
      append msg $indent "1$type value"
    } else {
      append msg $indent "$cmin$type values"
    }
  } else {
    append msg $indent "between $cmin and " \
	"$cmax$type values"
  }
  return $msg
}
########################################################################
proc ::clig::usage {_spec argv0 opt} {
  upvar $_spec spec
  #puts >>>>>>>>>>>[info level 0]
  #parray spec

  if {![info exist spec(fixed)]} {
    set spec(fixed) {}
  }
  if {[string length $opt]} {
    append msg "$argv0: unknown option `$opt'\n\n"
  }
  append msg "usage: $argv0"

  ## write the fixed args to the synopsis while finding the longest name
  set min 0
  foreach {var u} $spec(fixed) {
    append msg " " $var
    set l [string length $var]
    if {$l>$min} {set min $l}
  }

  ## write the synopsis while finding the longest option string
  set opts {}
  foreach opt $spec(opts) {
    if {[string match "--" $opt]} continue
    if {[string match Flag $spec($opt,type)]} {
      set v {}
    } else {
      set v " $spec($opt,var)"
    }
    if {[info exist spec($opt,mandatory)]} {
      append msg " $opt$v"
    } else {
      append msg " \[$opt$v\]"
    }
    set l [string length $opt]
    if {$l>$min} {set min $l}
  }

  ## write synopsis for Rest
  set haveRest 0
  if {[info exist spec(--,type)]} {
    foreach {rcmin rcmax} $spec(--,count) break
    ## There is an entry for Rest aka `--'
    set haveRest 1
    append msg " \[--\] "
    if {$rcmin==0} { append msg "\[" }
    append msg $spec(--,var)
    if {$rcmax>1} { append msg " ..." }
    if {$rcmin==0} { append msg "\]" }
    set L [string length $spec(--,var)]
    if {$L>$min} {set min $L}
  }

      
  ## write on-line usage string of the function
  if {[info exist spec(usage)]} {
    set s [string trim $spec(usage) "\n \t"]
    regsub -all "\n *" $s "\n  " s
    append msg "\n  " $s
  }

  ## now we know min and can fix an indent for textual information
  #if {$min>10} {set min 10}
  set indent [format "\n%[expr {$min+4}]s" {}]

  ## write description for fixed parameters
  foreach {var u} $spec(fixed) {
    append msg [makeOptUsage $min $indent $var $u]
  }

  ## write the description for every option
  foreach opt $spec(opts) {
    if {[string match "--" $opt]} continue
    append msg [makeOptUsage $min $indent $opt $spec($opt,usage)]

    if {"$spec($opt,type)"=="Flag"} continue

    ## explain how many args are allowed
    append msg [makeOptCount $indent \
		    " $spec($opt,type)" $spec($opt,count)]

    ## explain the range of values (if applicable)
    if {[info exist spec($opt,range)]} {
      append msg " in the range \[" [join $spec($opt,range) ", "] "\]"
    }
    if {[info exist spec($opt,default)]} {
      append msg " with a default of `$spec($opt,default)'"
    }
  }

  ## write description for Rest, if applicable
  if {$haveRest} {
    append msg [makeOptUsage $min $indent $spec(--,var) $spec(--,usage)]
    append msg [makeOptCount $indent {} $spec(--,count)]
  }

  ## write version 
  if {[info exist spec(version)]} {
    append msg "\nVersion: " $spec(version)
  }
  
  return -code error $msg
} 
########################################################################
