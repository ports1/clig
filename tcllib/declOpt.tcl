########################################################################
#
# A helper function which is called from String, Float and Int.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.4 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## make sure, oldOpts.tcl can be found
set auto_index(::clig::oldOpts) \
    [list source [file join [file dir [info script]] oldOpts.tcl]]


namespace eval ::clig {}
########################################################################
proc ::clig::declOpt {Type opt var usage argv} {
  ## We enter the declaration into the variable the name of which is
  ## stored in ::clig::currentSpec. Locally we refer to it as just
  ## `spec'. 
  variable currentSpec
  upvar $currentSpec spec

  ## The given option may not have been given before
  if {[info exist spec($opt,type)]} {
    return -code error "option `$opt' specified twice"
  }
  
  #parray $numberSpec
  lappend spec(opts) $opt
  set spec($opt,type) $Type
  set spec($opt,var) $var
  set spec($opt,usage) $usage

  ## the specification for $Type (not the one we want to fill in) has
  ## the name ${Type}Spec. We bind it to typeSpec.
  variable ${Type}Spec 
  upvar 0 ${Type}Spec typeSpec

  ## Suppose typeSpec (which might refer to StringSpec or FloatSpec,
  ## etc) has an entry (-r,var) = range, then we want parseCmdline
  ## ultimately set spec($opt,range). But parseCmdline only sets
  ## `range', so we have to link range to spec($opt,range). (If you
  ## don't understand that, ... well I don't either :-)
  foreach x $typeSpec(opts) {
    #puts "upvar 0 spec($opt,$typeSpec($x,var)) $typeSpec($x,var)"
    upvar 0 spec($opt,$typeSpec($x,var)) $typeSpec($x,var)
  }

  ## Map options give as {bla=1 2 3} to the normal style
  set argv [oldOpts $argv]
  #puts <<$argv>>

  if {[catch {parseCmdline typeSpec \
		  ::clig::$Type [llength $argv] $argv} err]} {
    #global errorInfo
    #puts $errorInfo
    return -code error $err
  }

  if {[info exist default] && [info exist mandatory]} {
    append msg \
	"default and mandatory (-d and -m) cannot be used "\
	"both at the same time"
    return -code error $msg
  }
}
########################################################################
