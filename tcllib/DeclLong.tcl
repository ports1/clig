########################################################################
#
# Declare a command line option with arguments of type long.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.3 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

## make sure, declOpt.tcl can be found
set auto_index(::clig::declOpt) \
    [list source [file join [file dir [info script]] declOpt.tcl]]

namespace eval ::clig {
  namespace export Long

  variable LongSpec

  ## Long has fixed parameters
  set LongSpec(fixed) {
    opt {option string to define, like -bla}
    var {name of variable which shall hold the parameters given to -bla}
    usage {descriptive text for -bla}
  }

  set LongSpec(opts) {-c -r -d -m}

  ## Long has option -c
  set LongSpec(-c,type) Long
  set LongSpec(-c,var) count
  set LongSpec(-c,count) {2 2}
  set LongSpec(-c,range) {0 oo}
  set LongSpec(-c,default) {1 1}
  set LongSpec(-c,usage) {
    minimum and maximum number of arguments allowed for this option
  }

  ## Long has option -r
  set LongSpec(-r,type) Long
  set LongSpec(-r,var) range
  set LongSpec(-r,count) {2 2}
  set LongSpec(-r,range) {-oo oo}
  set LongSpec(-r,default) "-oo oo"
  set LongSpec(-r,usage) { 
    minimum and maximum value of arguments allowed for this option
  }

  ## Long has option -d
  set LongSpec(-d,type) String
  set LongSpec(-d,var) default
  set LongSpec(-d,count) {0 oo}
  set LongSpec(-d,usage) \
      {default(s) to substitute if option is not given} \

  ## Long has option -m
  set LongSpec(-m,type) Flag
  set LongSpec(-m,var) mandatory
  set LongSpec(-m,usage) \
      {request the defined "option" to be in fact mandatory}

  ## usage string
  set LongSpec(usage) {declare an option with args of type long}

}
########################################################################
proc ::clig::Long  {opt var usage args} {  
  if {[catch {declOpt Long $opt $var $usage $args} err]} {
    return -code error $err 
  }
}
########################################################################
