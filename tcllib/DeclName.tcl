########################################################################
#
# Declare the name of a program.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.3 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

namespace eval ::clig {
  namespace export Name

  variable NameSpec

  set NameSpec(fixed) {
    name {the name of the declared program/function}
  }
  set NameSpec(usage) {
    enters the name of the declared program/function into its spec
  }
}
########################################################################
proc ::clig::Name {name} {
  variable currentSpec
  upvar $currentSpec spec

  set spec(name) $name
}
########################################################################
