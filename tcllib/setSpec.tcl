########################################################################
#
# set name of spec-Variable to use in subsequent calls to Flag,
# String, etc.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.3 $, $Date: 2004/08/04 10:08:10 $
########################################################################
## source version and package require
source [file join [file dir [info script]] version.tcl]

namespace eval ::clig {
  namespace export setSpec
  variable currentSpec
}

proc ::clig::setSpec {varname {warn 1}} {
  if {![string match ::* $varname]} {
    append msg \
	"::clig::setSpec(warning) the given variable does not " \
	"have an absolute namespace-qualification. This may not work."
    puts stderr $msg
  }
  variable currentSpec $varname
}
########################################################################
