########################################################################
#
# Declare a variable where your program will find the whole command
# line as one string.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.3 $, $Date: 2004/08/04 10:08:09 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

namespace eval ::clig {
  namespace export Commandline

  variable CommandlineSpec

  set CommandlineSpec(fixed) {
    commandline {
      name of a variable where your program/function
      will find the whole command line as one string
    }
  }
  set CommandlineSpec(usage) {
    enters $commandline into spec of program/function
  }
}
########################################################################
proc ::clig::Commandline {commandline} {
  variable currentSpec
  upvar $currentSpec spec

  set spec(commandline) $commandline
}
########################################################################
