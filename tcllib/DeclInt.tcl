########################################################################
#
# Declare a command line option with arguments of type integer.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.3 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

## make sure, declOpt.tcl can be found
set auto_index(::clig::declOpt) \
    [list source [file join [file dir [info script]] declOpt.tcl]]

namespace eval ::clig {
  namespace export Int

  variable IntSpec

  ## Int has fixed parameters
  set IntSpec(fixed) {
    opt {option string to define, like -bla}
    var {name of variable which shall hold the parameters given to -bla}
    usage {descriptive text for -bla}
  }

  set IntSpec(opts) {-c -r -d -m}

  ## Int has option -c
  set IntSpec(-c,type) Int
  set IntSpec(-c,var) count
  set IntSpec(-c,count) {2 2}
  set IntSpec(-c,range) {0 oo}
  set IntSpec(-c,default) {1 1}
  set IntSpec(-c,usage) {
    minimum and maximum number of arguments allowed for this option
  }

  ## Int has option -r
  set IntSpec(-r,type) Int
  set IntSpec(-r,var) range
  set IntSpec(-r,count) {2 2}
  set IntSpec(-r,range) {-oo oo}
  set IntSpec(-r,default) "-oo oo"
  set IntSpec(-r,usage) { 
    minimum and maximum value of arguments allowed for this option
  }

  ## Int has option -d
  set IntSpec(-d,type) String
  set IntSpec(-d,var) default
  set IntSpec(-d,count) {0 oo}
  set IntSpec(-d,usage) \
      {default(s) to substitute if option is not given} \

  ## Int has option -m
  set IntSpec(-m,type) Flag
  set IntSpec(-m,var) mandatory
  set IntSpec(-m,usage) \
      {request the defined "option" to be in fact mandatory}

  ## usage string
  set IntSpec(usage) {declare an option with args of type integer}

}
########################################################################
proc ::clig::Int  {opt var usage args} {  
  #parray intSpec
  if {[catch {declOpt Int $opt $var $usage $args} err]} {
    return -code error $err 
  }
}
########################################################################
