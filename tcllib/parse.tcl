########################################################################
#
# The command line parser of clig.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.8 $, $Date: 2004/08/04 10:08:10 $
########################################################################
## source version and package require
source [file join [file dir [info script]] version.tcl]

## make sure, usage.tcl can be found
set auto_index(::clig::usage) \
    [list source [file join [file dir [info script]] usage.tcl]]

namespace eval ::clig {
  namespace export parseCmdline

  set parseCmdlineSpec(fixed) {
    _spec {reference to command line specification array}
    argv0 {name of calling function or program}
    argc {number of elements in argv}
    argv {parameter list}
  }

  ## parseCmdline has no optional arguments
  set parseCmdlineSpec(opts) {}
}
########################################################################
proc ::clig::isExtendedInt {v} {
  if {"$v"=="-oo" || "$v"=="oo"} {return 1}
  if {[catch {incr v 0}]} {
    return 0
  }
  return 1
}
########################################################################
proc ::clig::isExtendedFloat {v} {
  if {"$v"=="-oo" || "$v"=="oo"} {return 1}
  if {[catch {expr {$v+0.0}}]} {
    return 0
  }
  return 1
}
########################################################################
proc ::clig::inRange {v range} {
  foreach {min max} $range break
  return [expr {("$min"=="-oo" || $min<=$v) \
		    && ("$max"=="oo" || $v<=$max)}]
}
########################################################################
proc ::clig::parseCmdline {_spec argv0 argc argv} {
  upvar $_spec spec

  ## Although trivial and mostly useless, we include this here because 
  ## it is easier to implement than to write a disclaimer in the
  ## manual :-)
  if {[info exist spec(commandline)]} {
    upvar $spec(commandline) var
    set var $argv
  }

  set emsg ""
  for {set i 0} {$i<$argc} {incr i} {
    set opt [lindex $argv $i]
    #puts "$argv0: argv\[$i\]==$opt"
    if {![string match -* $opt]} {
      lappend rest $opt
      continue
    }
    if {"$opt"=="--"} {
      incr i
      #set rest [concat $rest [lrange $argv $i end]]
      eval lappend rest [lrange $argv $i end]
      set i $argc
      continue
    }

    if {![info exist spec($opt,type)]} {
      usage spec $argv0 $opt
    }

    #puts "upvar $spec($opt,var) var"
    upvar $spec($opt,var) var
    if {[info exist var]} {
      return -code error "Option `$opt' specified more than once"
    }

    if {"$spec($opt,type)"=="Flag"} {
      set var 1
      continue
    }
    foreach {cmin cmax} $spec($opt,count) break

    set var {}
    for {incr i;set c 1} \
	{$i<$argc && [inRange $c [list 0 $cmax]]} \
	{incr i; incr c} {
	  set val [lindex $argv $i]
	  #puts "val=$val"
	  ## now check if this is still valid
	  switch -- $spec($opt,type) {
	    String {
	      if {[string match -* $val]} break
	      if {[string match @* $val]} {
		set val [string range $val 1 end]
	      }
	    }
	    Long -
	    Int {
	      if {![isExtendedInt $val]} break
	      if {![inRange $val $spec($opt,range)]} {
         	append emsg "\nparameter '$val of option " \
		    "`$opt' is out of range=" \
		    "\[$spec($opt,range)\]"
		break
	      }
	    }
	    Double -
	    Float {
	      if {![isExtendedFloat $val]} break
	      if {![inRange $val $spec($opt,range)]} {
		append emsg "\nfloat param $val of option " \
		    "`$opt' is out of range="\
		    "\[$spec($opt,range)\]"
		break
	      }
	    }
	  }
	  lappend var $val
	} ;#for
    incr i -1
    
    set L [llength $var]
    if {![inRange $L [list $cmin oo]]} {
      append emsg "\nnot enough params for option $opt, " \
	  "found $L but need $cmin"
    }
  }

  ## Check availability of mandatory options
  foreach mtory [array names spec *,mandatory] {
    regsub {,mandatory$} $mtory {} opt
    upvar $spec($opt,var) var
    if {![info exist var]} {
      append emsg "\nmissing mandatory parameter `$opt'"
    }
  }

  ## Enter default values where necessary
  foreach dflt [array names spec *,default] {
    regsub {,default$} $dflt {} opt
    #puts "defaulting $dflt to $spec($dflt) into variable $spec($opt,var)"
    upvar $spec($opt,var) var 
    if {![info exist var]} {
      set var $spec($dflt)
    }
  }

  ## Handle rest
  if {[info exist spec(--,type)]} {
    if {![info exist rest]} {
      set l 0
    } else {
      set l [llength $rest]
    }
    if {![inRange $l $spec(--,count)]} {
      foreach {cmin cmax} $spec(--,count) break
      if {$l==0} {
	if {$cmin==1} {set s {}} else {set s s}
	append emsg \
	    "\nneed at least " $cmin " non-option argument" $s
      } else {
	if {$l==1} {set s {}} else {set s s}
	append emsg \
	  "\nfound $l non-option argument$s, namely `$rest', " \
	  "but I want "
	if {$cmin==$cmax} {
	  append emsg "$cmin."
	} else {
	  append emsg "between $cmin and $cmax."
	}
      }
    } else {
      if {$l>0} {
	upvar $spec(--,var) var
	set var $rest
      }
    }
  } else {
    if {[info exist rest]} {
      append emsg "\nsuperfluous non-option arguments `$rest'"
    }
  }
  #puts $rest
  if {""!="$emsg"} {
    set l [string length "$argv0: "]
    set p [format "% ${l}s" "o "]
    regsub -all "\n" $emsg "\n$p" emsg
    return -code error \
	"$argv0: the comand line contains errors$emsg"
  }
}
