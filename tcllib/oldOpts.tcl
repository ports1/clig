########################################################################
#
# Convert old-style clig-options like count=2,3 into their new
# counterparts, e.g. `-c 2 3'.
#
# $Revision: 1.2 $, $Date: 2000/01/04 14:59:34 $
#
########################################################################


namespace eval ::clig {}

########################################################################
#
# The following conversions are performed:
# `mandatory' --> `-m'
# `default=<a>' --> '-d <a>
# `count=<a>,<b>' --> `-c <a> <b>
# `range=<a>,<b>' --> `-r <a> <b>
#
proc ::clig::oldOpts {argv} {
  set res {}
  foreach a $argv {
    if {[regexp {^ *mandatory *$} $a]} {
      lappend res -m
      continue
    }
    if {[regexp {^ *default *=(.*)} $a dummy value]} {
      lappend res -d
      foreach x $value {
	if {[string match -* $x]} {
	  lappend res @$x
	} else {
	  lappend res $x
	}
      }
      continue
    }
    if {[regexp {^ *count *=(.*),(.*)} $a dummy lo hi]} {
      lappend res -c [string trim $lo] [string trim $hi]
      continue
    }
    if {[regexp {^ *range *=(.*),(.*)} $a dummy lo hi]} {
      lappend res -r [string trim $lo] [string trim $hi]
      continue
    }
    lappend res $a
  }
  return $res
}
########################################################################

if 0 {
  namespace eval ::clig {
    puts [parseEqStyle {bla -x} {-blo 88 {bla=ij kl mn}}]
  }
}
