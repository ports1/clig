########################################################################
#
# Declare a command line option flag.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.4 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

namespace eval ::clig {
  namespace export Flag

  variable FlagSpec

  set FlagSpec(fixed) {
    opt {option string to define, like -bla}
    var {
      name of variable to set to 1 if -bla is on the command line. If
      -bla is not on the command line, the variable will not be set.
    }
    usage {descriptive text for -bla}
  }
  
  ## Flag has no options
  set FlagSpec(opts) {}

  ## usage string
  set FlagSpec(usage) {declare a Flag option}
}
########################################################################
#
proc ::clig::Flag {opt var usage} {
  variable currentSpec
  upvar $currentSpec spec

  if {[info exist spec($opt,type)]} {
    return -code error "option `$opt' specified twice"
  }

  lappend spec(opts) $opt
  set spec($opt,type) Flag
  set spec($opt,var) $var
  set spec($opt,usage) $usage
}
########################################################################
 
