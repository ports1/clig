########################################################################
#
# Declare a `usage:'-oneliner for a function
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.3 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

namespace eval ::clig {
  namespace export Usage

  variable UsageSpec

  set UsageSpec(fixed) {
    oneliner {short description of what the function described by does}
  }
  set UsageSpec(usage) {
    enters a short description into a function spec
  }
}
########################################################################
proc ::clig::Usage {oneliner} {
  variable currentSpec
  upvar $currentSpec spec

  set spec(usage) $oneliner
}
########################################################################
