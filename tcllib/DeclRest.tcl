########################################################################
#
# Function to specify non-option arguments on the command line
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.4 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

## make sure, declOpt.tcl can be found
set auto_index(::clig::declOpt) \
    [list source [file join [file dir [info script]] declOpt.tcl]]

namespace eval ::clig {
  namespace export Rest

  variable RestSpec

  ## Rest has fixed params
  set RestSpec(fixed) {
    var {name of variable to receive non-option arguments}
    usage {description of non-option arguments}
  }

  set RestSpec(opts) {-c}

  ## Rest has option -c
  set RestSpec(-c,type) Int
  set RestSpec(-c,var) count
  set RestSpec(-c,count) {2 2}
  set RestSpec(-c,range) {0 oo}
  set RestSpec(-c,default) {0 oo}
  set RestSpec(-c,usage) \
      {declar minimum and maximum allowed number of non-option arguments} 

  ## usage string
  set RestSpec(usage) {declare the number of non-option arguments}
}
########################################################################
proc ::clig::Rest {var usage args} {
  if {[catch {declOpt Rest -- $var $usage $args} err]} {
    return -code error $err
  }
}
########################################################################
