########################################################################
#
# Declare the version of a program.
#
# (C) 1999-2004 Harald Kirsch (clig@geggus.net)
#
# $Revision: 1.3 $, $Date: 2004/08/04 10:08:10 $
########################################################################

## source version and package require
source [file join [file dir [info script]] version.tcl]

namespace eval ::clig {
  namespace export Version

  variable VersionSpec

  set VersionSpec(fixed) {
    version {the version of the declared program/function}
  }
  set VersionSpec(usage) {
    enters the version of the declared program/function into its spec
  }
}
########################################################################
proc ::clig::Version {version} {
  variable currentSpec
  upvar $currentSpec spec

  set spec(version) $version
}
########################################################################
