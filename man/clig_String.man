.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_String.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::String \- declare an option with parameters of type string

.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*
.BI setSpec " db"
.fi

.BI "String -" "opt varname usage" 
.RB [ -c
.IR "min max" ]
.RB {[ -d
.IR "default ..." ]
.RB "| [" -m ]}

.\"*********************************************************************
.SH DESCRIPTION


The
.B String
command declares
.I -opt
to have zero or more string arguments.

.IP varname
declares that arguments found for 
.I -opt 
are passed to the caller of the parser in a variable (tcl) or slot (C) 
of that name.

.IP usage
is a short text describing 
.IR -opt .
It is used in the generated manual or in a usage-message printed by
the parser if necessary.

.IP -c
specifies the minimum and maximum number of parameters
.I -opt 
may have. If less than \fImin\fR or more than \fImax\fR are found, the 
parser prints an error message to stderr and terminates the calling
process. 
Use \fImin\fR==\fImax\fR to request exactly so many arguments. As a
special value for \fBmax\fR, \fBoo\fR is understood as positive infinity.

.IP -d
stores a (list of) default value(s) for option
.IR -opt .
The parser pretends these were found on the command line, if option
.I -opt
is not given. 
.br
\fBWARNING:\fR The number of default values is not
checked against the minimum and maximum values given for 
.BR -c .

.IP -m
declares that
.I -opt
is mandatory. Put another way, the ``option'' is not really optional
but must show up on the command line. If it does not, the parser
prints an error message to stderr and exits the calling process. This
can not be used together with 
.BR -d .


.SH "PARSER RESULT"
.SS "tcl"
Within a Tcl-script, the parser will set the variable with name
.I varname
in the stack-frame of its caller if and only if option
.I -opt
is found on the command line or has a default value.

.SS C
The structure returned by the parser contains the slots
\fIvarname\fBP\fR, \fIvarname\fR and \fIvarname\fBC\fR.  The slot
\fIvarname\fBP\fR will be set to a non-zero value, if and only if the
option
.I -opt
is found on the command line or has a default value. Otherwise it will 
be 0. The slot \fIvarname\fBC\fR will be set to the number of
arguments found for
.IR -opt .
If the number of arguments for
.I -opt
is allowed to be greater than 1, slot \fIvarname\fR has type
\fBchar**\fR, otherwise it has type \fBchar*\fR.

.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|
