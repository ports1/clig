.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_Version.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::Version \- declare version
.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*
.BI setSpec " db"

.BI Version " version"
.fi

.\"*********************************************************************
.SH DESCRIPTION
The
.B Version
command specifies a string to be used as a version-identification of
the generated parser. Currently it is merely printed as part of the
usage-information. An example using my favorite type of version
numbers would be

.RS 2
.nf
Version 2021-06-04
.fi
.RE

.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|

