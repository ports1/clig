.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_Usage.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::Usage \- declare single line usage-string
.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*
.BI setSpec " db"

.BI Usage " one-liner"
.fi

.\"*********************************************************************
.SH DESCRIPTION

The 
.B Usage
command specifies a one line description of the program's intent and
function. The string
.I one-liner
is printed --- besides other sutff --- if the parser encounters an
unknown option.
Remember to enclose 
.I one-liner
in braces. A (not so) typical example would be

.RS 2
.nf
Usage {program to delete your home directory}
.fi
.RE

In fact, the
.I one-liner
can be longer than one line. Best results are obtained, if
.I one-liner 
is not put in braces but is surrounded by double quotes. The quoted
string may than contain newlines. Clig automatically adds backslashes
where necessary in the generated C-code so that the C-compiler does
not go wild.



.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|
