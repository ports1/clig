.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_Rest.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::Rest \- declare command line arguments not associated with any
option 
.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*
.BI setSpec " db"
.fi

.BI Rest " name usage"
.RB [ -c
.IR "lo hi" ]


.\"*********************************************************************
.SH DESCRIPTION

The
.B Rest
command specifies, what the parser should do with command line
arguments not associated with any option. Option
.B -c
specifies how man non-option arguments are allowed on the command line 
(see clig::String(n)). The default values for
.IR lo " and " hi
are 0 and 
.B oo
respectively.

Without a 
.BR Rest -declaration
the parser will generate an error message
and terminate the program. 

If a
.B Rest
is declared, non-option arguments are returned by the parser as
described below.

.SH  "PARSER RESULT"
.SS "tcl"
Within a Tcl-script, the parser will set the variable with name
.I varname
in the stack-frame of its caller to the arguments on the command line
which are not parameters of any option.

.SS C
Mainly for historical reasons, the structure returned by the parser
contains the slots \fBint argc\fR and \fBchar **argv\fR. They contain
the arguments on the command line
which are not parameters of any option.

Example use of
.BR Rest :

.RS 2
.nf
Rest infiles {input files} -c 1 oo
.fi
.RE
.RE

.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|
