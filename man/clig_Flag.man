.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_Flag.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::Flag \- declare a flag (boolean, classic) option

.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*
.BI setSpec " db"

.BI "Flag -" "opt varname usage"
.fi

.\"*********************************************************************
.SH DESCRIPTION

The
.B Flag
command declares an option without any parameters. The command line
parser merely checks for the existence of
.I opt
on the command line. 

.SS tcl
If
.B parseCmdline
finds
.I opt
on the command line, variable
.I varname
in the caller's context is set to 1. If 
.I opt
is not on the command line, the variable is not set.

.SS C

If
.I opt
is found on the command line the slot
.IB variable P
in the structure returned by the parser will be
set to a non-zero value, otherwise it will be 0.

Example use of
.BR Flag :

.RS 2
.nf
Flag -v verbose {verbose operation}
.fi
.RE




.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|
