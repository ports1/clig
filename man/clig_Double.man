.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_Double.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::Double \- declare an option with parameters of type double

.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*
.BI setSpec " db"
.fi

.BI "Double -" "opt varname usage" 
.RB [ -c
.IR "min max" ]
.RB {[ -d
.IR "default ..." ]
.RB "| [" -m ]}
.RB [ -r
.IR "rmin rmax" ]

.\"*********************************************************************
.SH DESCRIPTION


The
.B Double
command works exactly as the 
.B Float 
command except that the generated C parser works with values of type
.B double
instead of
.BR float .
For the Tcl parser it does not make a difference.
.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|
