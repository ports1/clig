.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_Commandline.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::Commandline \- declare variable to store concatenated argv
.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*
.BI setSpec " db"

.BI Commandline " varname"
.fi

.\"*********************************************************************
.SH DESCRIPTION

The command
.B Commandline
records in the database
.I db
specified in the most recent call to 
.B setSpec
the name 
.I varname 
of a variable to be set the concatenated elements of the
command line by a clig-parser.

.SS Example
With a declaration like

.nf
  Commandline tool
.fi

a C-program calling

.nf
   Cmdline *cmd = parseCmdline(argc, argv);
.fi

will find in 
.B cmd->tool
all elements of argv joined into one string with intervening blanks.

.SH BUG
This even works with the command line parser for Tcl,
.BR ::clig::parseCmdline ,
although in contrast to C it is rather useless in Tcl.


.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|
