.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_Float.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::Float \- declare an option with parameters of type float

.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*
.BI setSpec " db"
.fi

.BI "Float -" "opt varname usage" 
.RB [ -c
.IR "min max" ]
.RB {[ -d
.IR "default ..." ]
.RB "| [" -m ]}
.RB [ -r
.IR "rmin rmax" ]

.\"*********************************************************************
.SH DESCRIPTION


The
.B Float
command declares
.I -opt
to have zero or more floating point arguments. The parameters
.IR variable " and " usage
as well as the options
.BR -c ", " -d " and " -m "
are described in
.BR clig::String (n).

.IP -r
instructs the parser to check that arguments given to
.I -opt
are within the inclusive range between
.IR rmin " and " rmax .
Special values for 
.IR rmin " and " rmax
are 
.BR "-oo" " and " oo"
denoting negative and positive infinity thereby not constraining the
parameters of
.BI -opt .
.PP

.B WARNING:
The implementation of option 
.B -d
is currently incomplete insofar, as the specified default values are
not checked by 
.BR clig ,
i.e. the following would go unnoticed:

.RS
.B "-d 1.0 -r 500.0 600.0"
.RE

In this case, if
.I option
is not on the command line, the clig parser will set variable
.I varname
for its caller to the out-of-range value 1.0. (Maybe this is a feature?)


Example use of
.BR Float :

.RS 2
.nf
Float -p prob {list of probabilities adding up to one} \\
    -r 0.0 1.0  -d 0.1 0.2 0.7 \\
    -c 1 oo
.fi
.RE

Please note, that the additional constraint requiring the values
summing to 1.0 cannot be checked by the parser.

.SH "PARSER RESULT"
.SS "tcl"
Within a Tcl-script, the parser will set the variable with name
.I varname
in the stack-frame of its caller if and only if option
.I -opt
is found on the command line or has a default value.

.SS C
The structure returned by the parser contains the slots
\fIvarname\fBP\fR, \fIvarname\fR and \fIvarname\fBC\fR.  The slot
\fIvarname\fBP\fR will be set to a non-zero value, if and only if the
option
.I -opt
is found on the command line or has a default value. Otherwise it will 
be 0. The slot \fIvarname\fBC\fR will be set to the number of
arguments found for
.IR -opt .
If the number of arguments for
.I -opt
is allowed to be greater than 1, slot \fIvarname\fR has type
\fBfloat*\fR, otherwise it has type \fBfloat\fR.

.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|
