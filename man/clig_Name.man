.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_Name.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::Name \- set the program name to be used in the manual page
.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*
.BI setSpec " db"

.BI Name " program-name"
.fi

.\"*********************************************************************
.SH DESCRIPTION
The
.BR Name -command
specifies the name of the program. The string
.I program-name
is used in the generated manual page and also specifies the default
name of the manual page. However the specified name is not used
anywhere else in the parser because use of argv[0] is preferred there.


.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|
