.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_parseCmdline.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::parseCmdline \- command line interpreter for Tcl
.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*

.BI setSpec " var"
.BI parseCmdline " _spec argv0 argc argv"
.fi

.\"*********************************************************************
.SH DESCRIPTION
This manual page describes how to instrument your Tcl-scripts with a
command line parser. It requires that package clig is installed
whenever your script is run. To find out how to create a parser which
is independent of clig, read clig(1). (Well, don't, it is not yet
implemented.) 

The options to be understood by your script must be declared with
calls to ::clig::Flag, ::clig:String etc., which is best done in a
separate file, e.g. cmdline.cli, to be sourced by your script. Having
these declarations in a separate file allows you to run 
.BR clig ,
the program, on that file to create a basic manual page.


.SS setSpec
The option-declaring functions want to store the declarations in an
array with a globally accessible name. For compatibility with older
software, the name of this array can not be passed as a
parameter. Consequently, your script must declare it before sourcing 
.BR cmdline.cli .

A call like

.nf
   setSpec ::main
.fi

declares 
.B ::main
as the database (array) to be filled by subsequent calls to
::clig::Flag and the like. The array should not contain anything
except entries created with the declarator functions.

.SS parseCmdline
After declaring the database and sourcing your declarations from
.BR cmdline.cli
your script is ready to call parseCmdline. This is typically done with:

.nf
  set Program [file tail $argv0]
  if {[catch {parseCmdline ::main $Program $argc $argv} err]} {
    puts stderr $err
    exit 1
  }
.fi

If
.B parseCmdline
finds an unknown option in 
.BR $argv ,
it prints a usage-message to stderr
and exits the script with exit-code 1. If it finds any other errors,
like numerical arguments being out of range, it prints an appropriate
error-message to stderr and also exits your script with code
1. Setting 
.B Program
and passing it to
.B parseCmdline 
instead of
.B $argv0
results in nicer error-messages.

If no errors occur, 
.B parseCmdline 
enters the values found into variables of its callers context. The
names of these variables are those declared with the declarator
functions. For example with a declaration like

.nf
  Float -ival ival {interval to take into account} -c 2 2 
.fi

the caller will find the variable 
.B ival
set to a list with 2 elements, if option
.B -ival
was found with two numeric arguments in 
.BR $argv .

If option
.B -ival
is not existent on the given command line, 
.B ival
will not be set. Consequently, it is best to not set the declared
variables to any value before calling
.BR parseCmdline .

.SS Summary
The typical skeleton of your script should look like

.nf
  package require clig
  namespace import ::clig::*
  
  setSpec ::main
  source [file join path to your installed base cmdline.cli]
  set Program [file tail $argv0]
  if {[catch {parseCmdline ::main $Program $argc $argv} err]} {
    puts stderr $err
    exit 1
  }

.fi

.SH REMARK
Of course
.B parseCmdline
can be called from a within any 
.B proc 
with a specification database previously filled with the declarator
functions. I am not using 
.B OptProc
because it is not documented and the declaration syntax used here was
used for C-programs probably long before it existed.


.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|
