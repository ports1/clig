.\" (c) 1994-2004 by Harald Kirsch (clig@geggus.net)
.\" (c) 2004-2021 by Chris Hutchinson (portmaster_AT_bsdforge.com)
.\"
.\" $Id: clig_Description.man,v 1.4 2021/06/04 11:18:14 chris.h Exp $
.\"
.\" -----------------------------------------------------------------
.TH CLIG 1  "|Version|" "" "Programmer's Manual"
.SH NAME
::clig::Description \- set long description text to be included in a
manual page
.SH SYNOPSIS
.nf
package require clig
namespace import ::clig::*
.BI setSpec " db"

.BI Description " text"
.fi

.\"*********************************************************************
.SH DESCRIPTION

The
.BR Description
command should not be used. Instead, the respective section in the
generated manual page should be filled out. The main reason for this
advice is, that clig copies
.I description
AS IS into the manual page, so it can be typed into the manual page
file in the first place. Please remember that the generated manual
page needs some hand-tuning anyway, because for example the `SEE
ALSO'-section cannot be generated. Since this command should not be
used, no example is given.


.\"*********************************************************************
.SH "SEE ALSO"
|SEEALSO|
