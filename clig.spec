## $Revision: 1.7 $, $Date: 2004/08/04 10:07:55 $
## The following line is edited by my ship script to contain the true
## version I am shipping from cvs. (kir)
%define VERSION 77.66.55

Summary: Command Line Interpreter Generator
Name: clig
Version: %VERSION
Release: 0
Copyright: GPL
Group: Development/Tools
Source: clig-%{VERSION}.tar.gz
URL: http://wsd.iitb.fhg.de/~geg/clighome/
Packager: Sven Geggus <clig@geggus.net>

Requires: tcl >= 8.0

BuildRoot: /tmp/clig-rpmbuild

%description
Handle command lines for C-programs or Tcl-scripts. May work for C++. 

Based on a simple specification file clig generates C-code for a function

        Cmdline *parseCmdline(int argc, char **argv)

which parses the command-line of a typical C-program and returns the
result in a structure of type Cmdline. Besides parseCmdline(), the
function

        void usage(void)

is generated and will be called by the command-line parser if the
command line contains obvious errors. One of the main reasons to use
clig is the automatic generation of a usage()-function which is
always up-to- date with respect to the options actually understood by
the program. Additionally, clig creates a manual page.

The package also contains the Tcl-package ::clig which can be used to
instrument Tcl-scripts with a command line very similar to what is
described above.

%prep
%setup

%build
make prefix=/usr

%install
rm -rf $RPM_BUILD_ROOT/usr/
make install build_root=$RPM_BUILD_ROOT prefix=/usr \

%post
## Fix the path to the tclsh
# Actually I would like to do the following, but it does not work due
# to rpms database locking (stupid)
# TCLSH=#\!`rpm -ql tcl|grep /tclsh$`
set -x
TCLSH=#\!`which tclsh`
p=/usr/bin
for f in clig cligUsage; do 
  cp $p/$f $p/$f.orig
  sed -e "1s,.*,$TCLSH," $p/$f.orig >$p/$f
  rm $p/$f.orig
done

%files
%attr(-,root,root) /usr/doc/clig-%{VERSION}
%attr(-,root,root) /usr/lib/clig-%{VERSION}
%attr(-,root,root) /usr/man/man1
%attr(-,root,root) /usr/man/mann
%attr(-,root,root) /usr/bin
