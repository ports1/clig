########################################################################
#
# (C) 1995-2004 Harald Kirsch
# (C) 2004-2021 Chris Hutchinson
#
########################################################################
include .version

# Pseudo root used during installation when preparing an installed
# package like an rpm.
build_root =

prefix =/usr
exec_prefix =$(prefix)

# Name of subdirectory to be used beyond DOCDIR and LIBDIR
cligdir =clig-$(VERSION)

# Directory in which to install the program clig
BINDIR =$(exec_prefix)/bin

# Directory in which to install scripts and helper files used by clig
# as well as the tcl-package ::clig.
# PLEASE NOTE: This directory must be found in your tcl's installation 
# search path, which can be set with the environment variable
# TCLLIBPATH. 
LIBDIR =$(prefix)/lib/$(cligdir)

# Directory in which to install manual entry for clig, the program
M1SUFFIX =1
MAN1DIR =$(prefix)/man/man$(M1SUFFIX)

# Directory in which to install manual entries for clig, the Tcl-package.
MnSUFFIX =n
MANnDIR =$(prefix)/man/man$(MnSUFFIX)

# Directory were to put release notes, changeLogs, News etc.
DOCDIR =$(prefix)/doc/$(cligdir)

TCLSH=/usr/bin/tclsh

######################################################################## 
#
# You shouldn't normally need to edit anything below
#
########################################################################

M1FILES = man/clig.man
M1FILES.f = $(M1FILES:%.man=%.mfixed)
MnFILES = \
  man/clig_Commandline.man \
  man/clig_Description.man \
  man/clig_Double.man \
  man/clig_Flag.man \
  man/clig_Float.man \
  man/clig_Int.man \
  man/clig_Long.man \
  man/clig_Name.man \
  man/clig_Rest.man \
  man/clig_String.man \
  man/clig_Usage.man \
  man/clig_Version.man \
  man/clig_parseCmdline.man
MnFILES.f = $(MnFILES:%.man=%.mfixed)

## How to fix manual pages
.SUFFIXES: $(SUFFIXES) .mfixed .man
.man.mfixed:
	$(TCLSH) ./man/fixman $< $(VERSION) $(DOCDIR) $(M1SUFFIX) $(MnSUFFIX)

## This make everthing. In fact only some strings must be edited into
## several files  
all: pkgIndex.tcl clig.fixed cligUsage.fixed $(M1FILES.f) $(MnFILES.f)

pkgIndex.tcl: $(PKGFILES)
	$(TCLSH) ./mkpkgindex

clig.fixed: tcl/clig pkgIndex.tcl
	$(TCLSH) ./trivsed -i tcl/clig -o $@ \
            'BASE \|BASE\|' 'BASE $(LIBDIR)' \
            '^#\!/bin/sh' "#!$(TCLSH)"

cligUsage.fixed: cligUsage pkgIndex.tcl
	$(TCLSH) ./trivsed -i cligUsage -o $@ \
            '^#\!/bin/sh' "#!$(TCLSH)"

## This one is cute:
man/clig.man: tcl/cmdline.cli
	./tcl/clig -t man -m man/clig -mx .man $<

clean:
	rm -f pkgIndex.tcl *.fixed $(M1FILES.f) $(MnFILES.f)

install: all
	$(TCLSH) ./install \
	   BINDIR=$(build_root)$(BINDIR) \
	   LIBDIR=$(build_root)$(LIBDIR) \
           MAN1DIR=$(build_root)$(MAN1DIR) M1FILES="$(M1FILES.f)" \
           M1SUFFIX=$(M1SUFFIX) \
	   MANnDIR=$(build_root)$(MANnDIR) MnFILES="$(MnFILES.f)" \
	   MnSUFFIX=$(MnSUFFIX) \
	   DOCDIR=$(build_root)$(DOCDIR) VERSION=$(VERSION)
