static void
missingErr(char *opt)
{
  fprintf(stderr, "%s: mandatory option `%s' missing\n",
	  Program, opt);
}
/**********************************************************************/
