**clig** -- a Command Line Interface Generator
```
        (C) 2004-2021 Chris Hutchinson <portmaster_AT_bsdforge.com>
        (C) 2004 Sven Geggus <clig@geggus.net>
        (C) 1995-2004 Harald Kirsch
```
What is clig? --- Read [ANNOUNCE](ANNOUNCE.md) to get an idea 

**How to install:**

After unpacking the file `clig.<version>.tar.gz` you'll find a directory
named "clig" containing this file.

1. Edit the top of `makefile` to suit your preferences. In particular
you'll want to edit the variable `prefix`.

2. Run `make`.
This will create `pkgIndex.tcl`, `clig.fixed` and `clig.1` .

3. Run `make install`.
This will copy the files to the places specified at the top of your
`makefile`.

4. Compile and run the trivial example program in the directory
`example` .

5. Read the manual page(s).

6. play with `cligUsage` and read its code to find out how to use
`::clig` within your [Tcl-scripts](https://www.tcl-lang.org/). Future revisions will contain an
updated manal page.

Bug reports and questions to https://gitlab.com/ports1/clig.
