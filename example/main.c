
#include <stdlib.h>

#include "cmdline.h"


int
main(int argc, char **argv)
{

  Cmdline *cmd = parseCmdline(argc, argv);
  showOptionValues();

  /***** 
    Normally you use cmd somewhere here instead of just calling
    showOptionValues. In this demo however, the c-compiler would
    complain without the following.
  *****/                         
  cmd->tool = cmd->tool;

  return EXIT_SUCCESS;
}
